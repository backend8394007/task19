const http = require('http');
const fs = require('fs');
const path = require('path');

const server = http.createServer((req, res) =>{
    let filePath = path.join(__dirname, req.url === '/car' ? 'index.html' : req.url);
    let extname = path.extname(filePath);
    let dataType = 'text/html';

    switch (extname){
        case '.js':
            dataType = 'text/javascript';
            break;
        case '.json':
            dataType = 'application/json'
            break;
        case '.png':
            dataType = 'image/png'
            break;
        case '.jpg':
            dataType = 'image/jpg'
            break;
    }

    fs.readFile(filePath, (err, data) =>{
        if (err){
            if (err.code === 'ENOENT'){
                fs.readFile(path.join(__dirname, '404.html'), (err, data) =>{
                    res.writeHead(200, {'Content-Type': 'text/html'});
                    res.end(data, 'utf8');
                });
            }else{
                res.writeHead(500);
                res.end(`Server Error: ${err.code}`);
            }
        }else{
            res.writeHead(200, {'Content-Type': 'text/html'});
            res.end(data, 'utf8');
        }
    });
});

const PORT = process.env.PORT || 3000;

server.listen(PORT, () => console.log(`server running on port: ${PORT}`));
